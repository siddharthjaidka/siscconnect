exports.MASTER_OBJECT = function (clientID, districtName, phone, password) {
    this.client_id = clientID;
    this.district_name = districtName;
    this.phone = phone;
    this.password = password;
    this.superintendent_array = [];
    this.hr_array = [];
    this.broker_consultant_array = [];
    this.primary_billing_finance_array = [];
    this.secondary_billing_finance_array = [];
};

exports.SUPERINTENDENT = function (name, email, phone, extension) {
    this.name = name;
    this.email = email;
    this.phone = phone;
    this.jobTitle = 'Superintendent';
    this.extension = extension;
};

exports.PRIMARY_BILLING_FINANCE = function (name, email, phone, extension) {
    this.name = name;
    this.email = email;
    this.phone = phone;
    this.jobTitle = 'Key Contact';
    this.extension = extension;
};

exports.BROKER_CONSULTANT = function (name, email, phone, extension) {
    this.name = name;
    this.email = email;
    this.phone = phone;
    this.jobTitle = 'Broker';
    this.extension = extension;
};

exports.HR = function (name, email, phone, extension) {
    this.name = name;
    this.email = email;
    this.phone = phone;
    this.jobTitle = 'Superintendent';
    this.extension = extension;
};

exports.SECONDARY_BILLING_FINANCE = function (name, email, phone, extension) {
    this.name = name;
    this.email = email;
    this.phone = phone;
    this.jobTitle = 'HR';
    this.extension = extension;
};

exports.NEW_AD_USER = function (name, mailNickname, upn) {
    this.accountEnabled = true;
    this.displayName = name;
    this.mailNickname = mailNickname;
    this.userPrincipalName = upn;
};

exports.NEW_AD_USER_PASSWORD = function (password) {
    this.forceChangePasswordNextSignIn = true;
    this.password = password;
    this.forceChangePasswordNextSignInWithMfa = false;
};

exports.NEW_AD_GROUP = function (displayName) {
    this.displayName = displayName;
    this.mailEnabled = false;
    this.mailNickname = displayName;
    this.securityEnabled = true;
};
