const XLSX = require('xlsx');
const needle = require('needle');
const _ = require('lodash');
const performance = require('perf_hooks').performance;
var workbook;
const figlet = require('figlet');
const models = require('./models');
const chalk = require('chalk');
const outputFileName = 'result.xlsx';
const async = require('async');
const generator = require('generate-password');
const masterSanitisedData = [];
const masterNewUserData = [];
var largeDataSet = [];
var startTime;
const mainsStartTime = performance.now();
var newEmailDomain = 'siscconnect.onmicrosoft.com';
const tokenURL = 'https://login.microsoftonline.com/siscconnect.onmicrosoft.com/oauth2/v2.0/token';
const groupsURL = 'https://graph.microsoft.com/v1.0/groups?$top=999';
console.log(chalk.cyanBright(figlet.textSync('SISCConnect AD Users Script', {
    font: 'Big Money-se',
    horizontalLayout: 'default',
    verticalLayout: 'default'
})));
const createGroupURL = 'https://graph.microsoft.com/v1.0/groups';
const addUserURL = 'https://graph.microsoft.com/v1.0/users';
function updateUserURL(userObjectID) { return `https://graph.microsoft.com/v1.0/users/${userObjectID}`; }
function addUserToGroupURL(groupID) { return `https://graph.microsoft.com/v1.0/groups/${groupID}/members/$ref`; }
const tokenPayload = {
    client_secret: 'RJdN4U4zey5sYv7S@/zFs[I=7B43-_aO',
    client_id: '1ef4eea2-1123-4e02-b2b7-70e3b617935e',
    grant_type: 'client_credentials',
    scope: 'https://graph.microsoft.com/.default'
};
const finalUserCreationData = [];


module.exports = function (filePath, domain) {
    workbook = XLSX.readFile(filePath, { type: 'binary' });
    this.newEmailDomain = domain;
    // Init
    console.log(chalk.magentaBright('Script Initialised Successfully!\n'));
    // pick all workseets from workbook, parse & append data to array
    console.log(`${chalk.whiteBright('Parsing Input Excel Sheet!')}`);
    startTime = performance.now();
    workbook.SheetNames.forEach((sheetName) => {
        const worksheet = workbook.Sheets[sheetName];
        const data = (XLSX.utils.sheet_to_json(worksheet, { header: 0, blankrows: false }));
        largeDataSet = largeDataSet.concat(data);
    });
    console.log(`${chalk.greenBright('Excel Sheet Parsing Finished!,')} ${chalk.yellowBright('Elapsed Time: ')}${chalk.yellowBright(timeConversion(startTime = performance.now() - startTime))}`);
    console.log(`${chalk.whiteBright('Grouping Sheet Payload By Client ID & Detect Unique Header Keys!')}`);
    const uniqHeaderKeys = _.union(...(_.map(largeDataSet, _.keys)));
    const groupedADUsers = _.groupBy(largeDataSet, element => element.ClientID);
    console.log(`${chalk.greenBright(`Grouping by Client ID Finished!, Unique Header Keys Detected, Unique Clients Count: ${chalk.magentaBright(_.keys(groupedADUsers).length)}`)}, ${chalk.yellowBright('Elapsed Time: ')}${chalk.yellowBright(timeConversion(startTime = performance.now() - startTime))}`);
    console.log(`${chalk.whiteBright('Fetching Access Token from AD!')}`);
    // fetch access token from ad
    needle.post(tokenURL, tokenPayload, (err, resp) => {
        if (!(!(!err))) {
            if (!(!resp.body.access_token)) {
                const access_token = resp.body.access_token;
                var options = {
                    headers: { 'Authorization': `Bearer ${access_token}` }
                };
                console.log(`${chalk.greenBright('Access Token Fetched Succesfully!,')} ${chalk.yellowBright('Elapsed Time: ')}${chalk.yellowBright(timeConversion(startTime = performance.now() - startTime))}`);
                console.log(`${chalk.whiteBright('Sanatising Excel Sheet Started!')}`);
                const uniqueCLients = _.keys(groupedADUsers);
                uniqueCLients.forEach(client => {
                    const newCLient = new models.MASTER_OBJECT(parseInt(client), groupedADUsers[client][0]['District Name'], String(groupedADUsers[client][0].Phone).replace(/[^0-9]/g, ''), generator.generate({ length: 15, numbers: true, uppercase: true }));
                    groupedADUsers[client].forEach(user => {
                        if (user['Superintendent Name'] && String(user['Superintendent Name']).trim().length != 0) {
                            newCLient.superintendent_array.push(new models.SUPERINTENDENT(
                                user['Superintendent Name'],
                                user['Superintendent E-Mail'],
                                user['Superintendent Phone Number'] ? String(user['Superintendent Phone Number']).replace(/[^0-9]/g, '') : undefined,
                                user['Superintendent Extension'] ? String(user['Superintendent Extension']).replace(/[^0-9]/g, '') : undefined
                            ));
                        }
                        if (user['Key Billing/Finance Contact Name'] && String(user['Key Billing/Finance Contact Name']).trim().length != 0) {
                            newCLient.primary_billing_finance_array.push(new models.PRIMARY_BILLING_FINANCE(
                                user['Key Billing/Finance Contact Name'],
                                user['Key Billing/Finance Contact E-Mail'],
                                user['Primary Billing/Finance Contact Phone Number'] ? String(user['Primary Billing/Finance Contact Phone Number']).replace(/[^0-9]/g, '') : undefined,
                                user['Primary Billing/Finance Contact Extension'] ? String(user['Primary Billing/Finance Contact Extension']).replace(/[^0-9]/g, '') : undefined
                            ));
                        }
                        if (user['Billing/Finance Contact Name'] && String(user['Billing/Finance Contact Name']).trim().length != 0) {
                            newCLient.secondary_billing_finance_array.push(new models.SECONDARY_BILLING_FINANCE(
                                user['Billing/Finance Contact Name'],
                                user['Billing/Finance Contact E-Mail'],
                                user['Secondary Billing/Finance Contact Phone Number'] ? String(user['Secondary Billing/Finance Contact Phone Number']).replace(/[^0-9]/g, '') : undefined,
                                user['Secondary Billing/Finance Contact Extension'] ? String(user['Secondary Billing/Finance Contact Extension']).replace(/[^0-9]/g, '') : undefined
                            ));
                        }
                        if (user['Human Resources Contact Name'] && String(user['Human Resources Contact Name']).trim().length != 0) {
                            newCLient.hr_array.push(new models.HR(
                                user['Human Resources Contact Name'],
                                user['Human Resources Contact E-mail'],
                                user['Human Resources Contact Phone Number'] ? String(user['Human Resources Contact Phone Number']).replace(/[^0-9]/g, '') : undefined,
                                user['Human Resources Contact Extension'] ? String(user['Human Resources Contact Extension']).replace(/[^0-9]/g, '') : undefined
                            ));
                        }
                    });
                    newCLient.superintendent_array = _.uniqWith(newCLient.superintendent_array, _.isEqual).filter(e => e.name && e.email).filter(e => { if (validateEmail(e.email.trim())) { return true; } else { console.log(`${chalk.redBright(`Invalid E-Mail: ${chalk.yellowBright(e.email)}`)}`); return false; } });
                    newCLient.primary_billing_finance_array = _.uniqWith(newCLient.primary_billing_finance_array, _.isEqual).filter(e => e.name && e.email).filter(e => { if (validateEmail(e.email.trim())) { return true; } else { console.log(`${chalk.redBright(`Invalid E-Mail: ${chalk.yellowBright(e.email)}`)}`); return false; } });
                    newCLient.secondary_billing_finance_array = _.uniqWith(newCLient.secondary_billing_finance_array, _.isEqual).filter(e => e.name && e.email).filter(e => { if (validateEmail(e.email.trim())) { return true; } else { console.log(`${chalk.redBright(`Invalid E-Mail: ${chalk.yellowBright(e.email)}`)}`); return false; } });
                    newCLient.hr_array = _.uniqWith(newCLient.hr_array, _.isEqual).filter(e => e.name && e.email).filter(e => { if (validateEmail(e.email.trim())) { return true; } else { console.log(`${chalk.redBright(`Invalid E-Mail: ${chalk.yellowBright(e.email)}`)}`); return false; } });
                    newCLient.broker_consultant_array = _.uniqWith(newCLient.broker_consultant_array, _.isEqual).filter(e => e.name && e.email).filter(e => { if (validateEmail(e.email.trim())) { return true; } else { console.log(`${chalk.redBright(`Invalid E-Mail: ${chalk.yellowBright(e.email)}`)}`); return false; } });
                    masterSanitisedData.push(newCLient);
                });
                console.log(`${chalk.greenBright(`Sanatising Excel Sheet Finished!`)} ${chalk.yellowBright('Elapsed Time: ')}${chalk.yellowBright(timeConversion(startTime = performance.now() - startTime))}`);
                console.log(`${chalk.whiteBright('Populating New AD USer Objects!')}`);
                masterSanitisedData.forEach(clientObject => {
                    const newADUSersSubObjects = [];
                    clientObject.superintendent_array.forEach(user => {
                        const newADUser = {};
                        newADUser.creationObject = new models.NEW_AD_USER(user.name, user.email.trim().split('@')[0], `${String(user.email.trim().split('@')[0])}@${newEmailDomain}`);
                        newADUser.creationObject.passwordProfile = new models.NEW_AD_USER_PASSWORD(clientObject.password);
                        newADUser.updationObject = { businessPhones: [String(user.phone)], extension: user.extension, jobTitle: user.jobTitle };
                        newADUSersSubObjects.push(newADUser);
                    });
                    clientObject.broker_consultant_array.forEach(user => {
                        const newADUser = {};
                        newADUser.creationObject = new models.NEW_AD_USER(user.name, user.email.trim().split('@')[0], `${String(user.email.trim().split('@')[0])}@${newEmailDomain}`);
                        newADUser.creationObject.passwordProfile = new models.NEW_AD_USER_PASSWORD(clientObject.password);
                        newADUser.updationObject = { businessPhones: [String(user.phone)], extension: user.extension, jobTitle: user.jobTitle };
                        newADUSersSubObjects.push(newADUser);
                    });
                    clientObject.hr_array.forEach(user => {
                        const newADUser = {};
                        newADUser.creationObject = new models.NEW_AD_USER(user.name, user.email.trim().split('@')[0], `${String(user.email.trim().split('@')[0])}@${newEmailDomain}`);
                        newADUser.creationObject.passwordProfile = new models.NEW_AD_USER_PASSWORD(clientObject.password);
                        newADUser.updationObject = { businessPhones: [String(user.phone)], extension: user.extension, jobTitle: user.jobTitle };
                        newADUSersSubObjects.push(newADUser);
                    });
                    clientObject.primary_billing_finance_array.forEach(user => {
                        const newADUser = {};
                        newADUser.creationObject = new models.NEW_AD_USER(user.name, user.email.trim().split('@')[0], `${String(user.email.trim().split('@')[0])}@${newEmailDomain}`);
                        newADUser.creationObject.passwordProfile = new models.NEW_AD_USER_PASSWORD(clientObject.password);
                        newADUser.updationObject = { businessPhones: [String(user.phone)], extension: user.extension, jobTitle: user.jobTitle };
                        newADUSersSubObjects.push(newADUser);
                    });
                    clientObject.secondary_billing_finance_array.forEach(user => {
                        const newADUser = {};
                        newADUser.creationObject = new models.NEW_AD_USER(user.name, user.email.trim().split('@')[0], `${String(user.email.split('@')[0])}@${newEmailDomain}`);
                        newADUser.creationObject.passwordProfile = new models.NEW_AD_USER_PASSWORD(clientObject.password);
                        newADUser.updationObject = { businessPhones: [String(user.phone)], extension: user.extension, jobTitle: user.jobTitle };
                        newADUSersSubObjects.push(newADUser);
                    });
                    masterNewUserData.push({ client_id: clientObject.client_id, district_name: clientObject.district_name, phone: clientObject.phone, users: newADUSersSubObjects });
                });
                console.log(`${chalk.greenBright(`New AD User Objects Population Finished!`)} ${chalk.yellowBright('Elapsed Time: ')}${chalk.yellowBright(timeConversion(startTime = performance.now() - startTime))}`);
                console.log(`${chalk.whiteBright('Fetching AD Groups Recursively!')}`);
                fetchGroupsRecursive([], null, options).then((adGroups) => {
                    if (!(!adGroups) && adGroups.length !== 0) {
                        const azureADGroups = _.map(adGroups, (group) => { return { name: parseInt(String(group.displayName).trim()), id: group.id }; });
                        const groupClientIDs = _.map(azureADGroups, 'name');
                        console.log(`${chalk.greenBright(`AD Groups Fetched Succesfully!, Total Count: ${chalk.magentaBright(adGroups.length)}`)}, ${chalk.yellowBright('Elapsed Time: ')}${chalk.yellowBright(timeConversion(startTime = performance.now() - startTime))}`);
                        console.log(`${chalk.whiteBright('Initilaising AD Users Creation!\n')}`);
                        const subPromisesArray = [];
                        options.headers['Content-type'] = 'application/json';
                        new Promise((resolveMaster) => {
                            masterNewUserData.forEach(clientUsersObject => {
                                if (_.includes(groupClientIDs, clientUsersObject.client_id)) {
                                    const groupID = _.find(azureADGroups, { name: clientUsersObject.client_id }).id;
                                    clientUsersObject.users.forEach(userObject => {
                                        subPromisesArray.push(function (resolve) {
                                            options.headers['Content-type'] = 'application/json';
                                            needle.post(addUserURL, userObject.creationObject, options, (err, resp) => {
                                                if (!(!(!err))) {
                                                    if (!(!resp.body.id)) {
                                                        console.log(`${chalk.greenBright(`Creating User Successful for Client ${clientUsersObject.client_id}, User E-Mail!: ${chalk.yellowBright(userObject.creationObject.userPrincipalName)}`)}`);
                                                        needle.patch(updateUserURL(resp.body.id), userObject.updationObject, options, (updateErr, updateResp) => {
                                                            if (!(!(!updateErr))) {
                                                                if (updateResp.statusCode == 204) {
                                                                    console.log(`${chalk.greenBright(`Updating User Successful for Client ${clientUsersObject.client_id}, User E-Mail!: ${chalk.yellowBright(userObject.creationObject.userPrincipalName)}`)}`);
                                                                    needle.post(addUserToGroupURL(groupID), { '@odata.id': `https://graph.microsoft.com/v1.0/directoryObjects/${resp.body.id}` }, options, (addUserToGroupErr, addUserToGroupResp) => {
                                                                        if (!(!(!addUserToGroupErr))) {
                                                                            if (addUserToGroupResp.statusCode == 204) {
                                                                                console.log(`${chalk.greenBright(`Adding User To Group Successful for Client ${clientUsersObject.client_id}, User E-Mail!: ${chalk.yellowBright(userObject.creationObject.userPrincipalName)}`)}\n`);
                                                                                finalUserCreationData.push({ ClientID: clientUsersObject.client_id, Name: userObject.creationObject.displayName, Email: userObject.creationObject.userPrincipalName, Password: userObject.creationObject.passwordProfile.password });
                                                                                resolve(null, true);
                                                                            } else {
                                                                                console.log(`${chalk.redBright(`Adding User To Group Failed for Client ${clientUsersObject.client_id}, User E-Mail!: ${chalk.yellowBright(userObject.creationObject.userPrincipalName)}`)}\n`);
                                                                                resolve(null, false);
                                                                            }
                                                                        } else {
                                                                            console.log(`${chalk.redBright(`Adding User To Group Failed for Client ${clientUsersObject.client_id}, User E-Mail!: ${chalk.yellowBright(userObject.creationObject.userPrincipalName)}`)}\n`);
                                                                            resolve(null, false);
                                                                        }
                                                                    });
                                                                } else {
                                                                    console.log(`${chalk.redBright(`Updating User Failed for Client ${clientUsersObject.client_id}, User E-Mail!: ${chalk.yellowBright(userObject.creationObject.userPrincipalName)}`)}\n`);
                                                                    resolve(null, false);
                                                                }
                                                            } else {
                                                                console.log(`${chalk.redBright(`Updating User Failed for Client ${clientUsersObject.client_id}, User E-Mail!: ${chalk.yellowBright(userObject.creationObject.userPrincipalName)}`)}\n`);
                                                                resolve(null, false);
                                                            }
                                                        });
                                                    } else {
                                                        console.log(`${chalk.redBright(`Creating User Failed for Client ${clientUsersObject.client_id}, User E-Mail!: ${chalk.yellowBright(userObject.creationObject.userPrincipalName)}`)}\n`);
                                                        resolve(null, false);
                                                    }
                                                } else {
                                                    console.log(`${chalk.redBright(`Creating User Failed for Client ${clientUsersObject.client_id}, User E-Mail!: ${chalk.yellowBright(userObject.creationObject.userPrincipalName)}`)}\n`);
                                                    resolve(null, false);
                                                }
                                            });
                                        });
                                    });
                                } else {
                                    console.log(`${chalk.yellowBright(`Ad Group Not Found, New Group Will Be Created!: ${chalk.yellowBright(clientUsersObject.client_id)}`)}`);
                                    subPromisesArray.push(function (resolve) {
                                        needle.post(createGroupURL, new models.NEW_AD_GROUP(String(clientUsersObject.client_id)), options, (createGroupErr, createGroupResp) => {
                                            if (!(!(!createGroupErr))) {
                                                if (!(!createGroupResp) && !(!createGroupResp.body.id)) {
                                                    const groupID = createGroupResp.body.id;
                                                    clientUsersObject.users.forEach(userObject => {
                                                        needle.post(addUserURL, userObject.creationObject, options, (err, resp) => {
                                                            if (!(!(!err))) {
                                                                if (!(!resp.body.id)) {
                                                                    console.log(`${chalk.greenBright(`Creating User Successful for Client ${clientUsersObject.client_id}, User E-Mail!: ${chalk.yellowBright(userObject.creationObject.userPrincipalName)}`)}`);
                                                                    needle.patch(updateUserURL(resp.body.id), userObject.updationObject, options, (updateErr, updateResp) => {
                                                                        if (!(!(!updateErr))) {
                                                                            if (updateResp.statusCode == 204) {
                                                                                console.log(`${chalk.greenBright(`Updating User Successful for Client ${clientUsersObject.client_id}, User E-Mail!: ${chalk.yellowBright(userObject.creationObject.userPrincipalName)}`)}`);
                                                                                needle.post(addUserToGroupURL(groupID), { '@odata.id': `https://graph.microsoft.com/v1.0/directoryObjects/${resp.body.id}` }, options, (addUserToGroupErr, addUserToGroupResp) => {
                                                                                    if (!(!(!addUserToGroupErr))) {
                                                                                        if (addUserToGroupResp.statusCode == 204) {
                                                                                            console.log(`${chalk.greenBright(`Adding User To Group Successful for Client ${clientUsersObject.client_id}, User E-Mail!: ${chalk.yellowBright(userObject.creationObject.userPrincipalName)}`)}\n`);
                                                                                            finalUserCreationData.push({ ClientID: clientUsersObject.client_id, Name: userObject.creationObject.displayName, Email: userObject.creationObject.userPrincipalName, Password: userObject.creationObject.passwordProfile.password });
                                                                                            resolve(null, true);
                                                                                        } else {
                                                                                            console.log(`${chalk.redBright(`Adding User To Group Failed for Client ${clientUsersObject.client_id}, User E-Mail!: ${chalk.yellowBright(userObject.creationObject.userPrincipalName)}`)}\n`);
                                                                                            resolve(null, false);
                                                                                        }
                                                                                    } else {
                                                                                        console.log(`${chalk.redBright(`Adding User To Group Failed for Client ${clientUsersObject.client_id}, User E-Mail!: ${chalk.yellowBright(userObject.creationObject.userPrincipalName)}`)}\n`);
                                                                                        resolve(null, false);
                                                                                    }
                                                                                });
                                                                            } else {
                                                                                console.log(`${chalk.redBright(`Updating User Failed for Client ${clientUsersObject.client_id}, User E-Mail!: ${chalk.yellowBright(userObject.creationObject.userPrincipalName)}`)}\n`);
                                                                                resolve(null, false);
                                                                            }
                                                                        } else {
                                                                            console.log(`${chalk.redBright(`Updating User Failed for Client ${clientUsersObject.client_id}, User E-Mail!: ${chalk.yellowBright(userObject.creationObject.userPrincipalName)}`)}\n`);
                                                                            resolve(null, false);
                                                                        }
                                                                    });
                                                                } else {
                                                                    console.log(`${chalk.redBright(`Creating User Failed for Client ${clientUsersObject.client_id}, User E-Mail!: ${chalk.yellowBright(userObject.creationObject.userPrincipalName)}`)}\n`);
                                                                    resolve(null, false);
                                                                }
                                                            } else {
                                                                console.log(`${chalk.redBright(`Creating User Failed for Client ${clientUsersObject.client_id}, User E-Mail!: ${chalk.yellowBright(userObject.creationObject.userPrincipalName)}`)}\n`);
                                                                resolve(null, false);
                                                            }
                                                        });
                                                    });
                                                } else {
                                                    console.log(`${chalk.redBright(`Creating AD Group Failed for Client ${clientUsersObject.client_id}`)}\n`);
                                                }
                                            } else {
                                                console.log(`${chalk.redBright(`Creating AD Group Failed for Client ${clientUsersObject.client_id}`)}\n`);
                                            }
                                        });
                                    });
                                }
                            });
                            resolveMaster(true);
                        }).then(() => {
                            async.series(subPromisesArray, function () {
                                console.log(`${chalk.greenBright(`AD Users Creation Succeeded!`)} ${chalk.yellowBright('Elapsed Time: ')}${chalk.yellowBright(timeConversion(startTime = performance.now() - startTime))}`);
                                if (finalUserCreationData.length !== 0) {
                                    console.log(`${chalk.whiteBright('Initilaising Excel Result Sheet Creation!')}`);
                                    const resultWorkbook = XLSX.utils.book_new();
                                    const resultWorkSheet = XLSX.utils.json_to_sheet(finalUserCreationData);
                                    XLSX.utils.book_append_sheet(resultWorkbook, resultWorkSheet, 'result');
                                    try {
                                        XLSX.writeFile(resultWorkbook, outputFileName);
                                        console.log(`${chalk.greenBright(`Excel Result Sheet Created Successfully!`)} ${chalk.yellowBright('Elapsed Time: ')}${chalk.yellowBright(timeConversion(startTime = performance.now() - startTime))}\n`);
                                    } catch (e) {
                                        console.log(`${chalk.redBright(`Excel Result Sheet Creation Failed!`)} ${chalk.yellowBright('Elapsed Time: ')}${chalk.yellowBright(timeConversion(startTime = performance.now() - startTime))}\n`);
                                    }
                                } else {
                                    console.log(`${chalk.yellowBright(`No AD User Creation Data Found!`)}`);
                                }
                                console.log(`${chalk.magentaBright(`Script Executed Successfully!`)} ${chalk.yellowBright('Elapsed Time: ')}${chalk.yellowBright(timeConversion(performance.now() - mainsStartTime))}`);
                            });
                        });
                    } else {
                        console.log(`${chalk.redBright('Error Occured while fetching AD User Groups!')}`);
                    }
                });
            } else {
                console.log(`${chalk.redBright('Error Occured while fetching access token!')}`);
            }
        } else {
            console.log(`${chalk.redBright('Error Occured while fetching access token!')}`);
        }
    });
};


// asynchronous tail recursive function to fetch AD Groups
async function fetchGroupsRecursive(users_array, nextLink, options) {
    users_merged_array = _.concat([], users_array);
    temp_url = groupsURL;
    if (!(!nextLink)) {
        temp_url = nextLink;
    }
    payload = await user_base_fetch(temp_url, options);
    if (payload.data) {
        users_merged_array = _.concat(users_merged_array, payload.data);
    }
    if (!(!payload.link)) {
        return fetchUsersRecursive(users_merged_array, payload.link, options);
    } else {
        return users_merged_array;
    }
}

// base function to fetch AD Groups (top = 999)
function user_base_fetch(temp_url, options) {
    return new Promise(resolve => {
        needle.get(temp_url, options, (err, resp) => {
            if (!(!(!err))) {
                if (!(!resp.body.value)) {
                    next_link = undefined;
                    if (!(!resp.body['@odata.nextLink'])) {
                        next_link = resp.body['@odata.nextLink'];
                    }
                    resolve({ data: resp.body.value, link: next_link });
                }
            }
        });
    });
}

// time conversion base function
function timeConversion(millisec) {
    var seconds = (millisec / 1000).toFixed(3);
    var minutes = (millisec / (1000 * 60)).toFixed(3);
    var hours = (millisec / (1000 * 60 * 60)).toFixed(3);
    var days = (millisec / (1000 * 60 * 60 * 24)).toFixed(3);
    if (seconds < 60) {
        return seconds + ' Seconds';
    } else if (minutes < 60) {
        return minutes + ' Minutes';
    } else if (hours < 24) {
        return hours + ' Hours';
    } else {
        return days + ' Days';
    }
}

// function to validate email
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@']+(\.[^<>()\[\]\\.,;:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}